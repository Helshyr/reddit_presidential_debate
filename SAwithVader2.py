################################################################################
# Projet de traitement de données appliquées aux sciences humaines et sociales #
# Reddit comments about the american presidential debate                       #
# Part 1 : Sentiment Analysis with VADER                                       #
# Version 2 : maintenant avec plus de traitement statistique                   #
# Daudré--Treuil Prunelle & Masse Oceane                                       #
################################################################################

from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

analyser = SentimentIntensityAnalyzer()

################################################################################
################################################################################


### Fonction sentiment_analyzer_scores
def sentiment_analyzer_scores(sentence):
    """
    Une fonction utilisant VADER pour analyser un message
    """

    score = analyser.polarity_scores(sentence)
    return score

### Fonction analyzer_csv
def analyzer_csv(path):
    """
    La fonction principale de l'analyse de sentiment, pour chaque commentaires
    d'un document donné, le dictionnaire généré par sentiment_analyzer_scores
    est récupéré, puis une moyenne des scores négatifs, neutres et positifs et
    effectuée.
    """

    df = pd.read_csv(path, sep = ",")

    comments_l = df["comments"]

    ### Analyse de sentiments

    sentiment_analysis_neg_total = 0
    sentiment_analysis_neu_total = 0
    sentiment_analysis_pos_total = 0
    sentiment_analysis_cpd_total = 0

    sentiment_analysis_neg_l = []
    sentiment_analysis_neu_l = []
    sentiment_analysis_pos_l = []
    sentiment_analysis_cpd_l = []

    for i in comments_l:
        sentiment = sentiment_analyzer_scores(i)
        sentiment_analysis_neg_total += sentiment.get('neg')
        sentiment_analysis_neu_total += sentiment.get('neu')
        sentiment_analysis_pos_total += sentiment.get('pos')
        sentiment_analysis_cpd_total += sentiment.get('compound')

        sentiment_analysis_neg_l.append(sentiment.get('neg'))
        sentiment_analysis_neu_l.append(sentiment.get('neu'))
        sentiment_analysis_pos_l.append(sentiment.get('pos'))
        sentiment_analysis_cpd_l.append(sentiment.get('compound'))

    ### Moyenne

    sentiment_analysis_neg = sentiment_analysis_neg_total / len(comments_l)
    sentiment_analysis_neu = sentiment_analysis_neu_total / len(comments_l)
    sentiment_analysis_pos = sentiment_analysis_pos_total / len(comments_l)
    sentiment_analysis_cpd = sentiment_analysis_cpd_total / len(comments_l)

    ### Plus de traitements statisitques

    #print(sentiment_analysis_neg_l)
    #print(sentiment_analysis_neu_l)
    #print(sentiment_analysis_pos_l)

    ### Dictionaire à retourner

    retm = {}
    retm["neg"] = sentiment_analysis_neg
    retm["neu"] = sentiment_analysis_neu
    retm["pos"] = sentiment_analysis_pos
    retm["cpd"] = sentiment_analysis_cpd

    retl = {}
    retl["neg"] = sentiment_analysis_neg_l
    retl["neu"] = sentiment_analysis_neu_l
    retl["pos"] = sentiment_analysis_pos_l
    retl["cpd"] = sentiment_analysis_cpd_l

    ret = {}
    ret["mean"] = retm
    ret["liste"] = retl

    return ret

### Fonction barycentre
def barycentre(ax, neg, neu, pos, title):
    """
    Une fonction affichant le barycentre entre les scores négatifs, positifs et
    neutres
    """

    ax.plot([0,1,0.5,0],[0,0, 0.86,0]) #Création du triangle
    ax.text(0,0,"neg", horizontalalignment='right', verticalalignment='top')
    ax.text(1,0,"pos", horizontalalignment='left', verticalalignment='top')
    ax.text(0.5,1,"neu", horizontalalignment='center', verticalalignment='bottom')

    x = neg*0 + pos*1 + neu*0.5 #Rajout du barycentre
    y = neg*0 + pos*0 + neu*0.86
    ax.scatter(x, y, s=100, marker="*")

    ax.axis('off') #Mise en forme
    ax.axis('equal')
    ax.set_title(title)

### Fontion cpdScale()
def cpdScale(ax, cpd1, cpd2, cpd3):
    """
    Une fonction affichant differents scores composées sur une échelle de -1 à 1
    """
    ax.plot([-1,1],[0,0]) #Création de l'axe
    ax.text(-1,0, "-1", horizontalalignment='right', verticalalignment='top')
    ax.text(1,0, "1", horizontalalignment='left', verticalalignment='top')

    ax.scatter(cpd1, 0, s=100, marker="*", label = "Trump : {:.3f}".format(cpd1)) #Rajout des points
    ax.scatter(cpd2, 0, s=100, marker="^", label="Biden : {:.3f}".format(cpd2))
    ax.scatter(cpd3, 0, s=100, marker="o", label="PresidentialDebate : \n{:.3f}".format(cpd3))

    ax.axis('off') #Mise en forme
    ax.axis('equal')
    ax.set_title("Les différentes composées")
    ax.legend()

### Fonction histoFreq
def histoFreq(ax, cpd, title):
    #histo, edgo = np.histogram(cpd, bins=10)
    #print(histo)
    #print(edgo)
    #ax.hist(histo)
    ax.hist(cpd, bins=20)


################################################################################
################################################################################

### Récupération des scores

trump_score = analyzer_csv("trump_comments.csv")
trump_mean = trump_score.get("mean")
trump_neg = trump_mean.get("neg")
trump_neu = trump_mean.get("neu")
trump_pos = trump_mean.get("pos")
trump_cpd = trump_mean.get("cpd")

trump_l = trump_score.get("liste")
trump_l_cpd = trump_l.get("cpd")

biden_score = analyzer_csv("biden_comments.csv")
biden_mean = biden_score.get("mean")
biden_neg = biden_mean.get("neg")
biden_neu = biden_mean.get("neu")
biden_pos = biden_mean.get("pos")
biden_cpd = biden_mean.get("cpd")

biden_l = biden_score.get("liste")
biden_l_cpd = biden_l.get("cpd")

presidential_debate_score = analyzer_csv("presidential_debate_comments.csv")
presidential_debate_mean = presidential_debate_score.get("mean")
presidential_debate_neg = presidential_debate_mean.get("neg")
presidential_debate_neu = presidential_debate_mean.get("neu")
presidential_debate_pos = presidential_debate_mean.get("pos")
presidential_debate_cpd = presidential_debate_mean.get("cpd")

presidential_debate_l = presidential_debate_score.get("liste")
presidential_debate_l_cpd = presidential_debate_l.get("cpd")

print(trump_l_cpd)
#print(biden_cpd)
#print(presidential_debate_cpd)

### Affichage des barycentres
ax=plt.subplot(2,4,1)
barycentre(ax, trump_neg, trump_neu, trump_pos, "r/Trump \nsubreddit")

ax=plt.subplot(2,4,2)
barycentre(ax, biden_neg, biden_neu, biden_pos, "r/Biden \nsubreddit")

ax=plt.subplot(2,4,3)
barycentre(ax, presidential_debate_neg, presidential_debate_neu, presidential_debate_pos, "r/PresidentialDebate \nsubreddit")

### Affichage des composées

ax=plt.subplot(2,4,4)
cpdScale(ax, trump_cpd, biden_cpd, presidential_debate_cpd)

### Affichage des liiiiiistes
ax=plt.subplot(2,4,5)
histoFreq(ax, trump_l_cpd, "bite")

ax=plt.subplot(2,4,6)
histoFreq(ax, biden_l_cpd, "bite")

ax=plt.subplot(2,4,7)
histoFreq(ax, presidential_debate_l_cpd, "bite")

ax=plt.subplot(2,4,8)
cpdScale(ax, trump_cpd, biden_cpd, presidential_debate_cpd)

plt.subplots_adjust(wspace = 1)
plt.show()
