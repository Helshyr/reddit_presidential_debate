#+TITLE: redQ : the REDdit Querier
#+OPTIONS: ^:nil 
#+LATEX_HEADER: \usepackage[margin=1.0in]{geometry}

#+LATEX: \newpage

* Presentation
  redQ is a simple python tool allowing to quickly query reddit to retrieve post informations. It works using =BeautifulSoup= and =requests=, requiring few dependencies. Finally, it is easily extensible, allowing the advanced user to retrieve other informations from reddit posts.

* Installation 
  First, clone the repository :

#+BEGIN_SRC sh
git clone https://gitlab.com/Aethor/redQ.git
#+END_SRC

Then, use pip to install required dependencies :

#+BEGIN_SRC sh
pip install -r requirements.txt
#+END_SRC

* Command line usage
  redQ is first intended to be used from the command line. Basic usage of redQ would be :

#+BEGIN_SRC sh
python3 redq.py "my query"
#+END_SRC

redQ prints its output to stdin in standard unix fashion, so you can redirect its output using shell operators. 
By default, the output format is json.

#+BEGIN_SRC sh
python3 redq.py "april fool's jokes" > my_file.json
#+END_SRC

The following table shows what options are available. Please refer to
=python3 redq.py --help= for more details.

| Option                  | Description                          | Example Usage                      |
|-------------------------+--------------------------------------+------------------------------------|
| =-h, --help=              | Print help and exit                  | python3 redq.py -h                 |
| =-f, --format=            | Output format (default to json)      | python3 redq.py -f xml             |
| =-mpn, --max-posts-nb=    | Max number of reddit posts to return | python3 redq.py -mpn 5             |
| =-st, --sort-type=        | Reddit sort type                     | python3 redq.py -st sort           |
| =-mcn, --max-comments-nb= | Max number of comment per pos        | python3 redq.py -mcn 7             |
| =-awl, --attr-white-list= | Whitelist for informations to fetch  | python3 redq.py -awl title content |
| =-abl, --attr-black-list= | Blacklist for informations to fetch  | python3 redq.py -abl score         |

The =-awl= and =-abl= options deserve their own explanation. By default, the script outputs every information it can find for each fetched post. The list of informations to retrieve is stored inside the =final_attr_white_list= list, that is computed by taking the =attr_white_list= list, and removing from it every element in the =attr_black_list= list. By default, =attr_white_list= contains every possible informations to fetch (this list of information can be retrieved with =python3 redq.py -h=, but at the time of this writing it is composed of =title=, =content=, =score= and =comments=), while =attr_black_list= is empty. Respectively, the =-awl= and =-abl= options can be used to set those lists.

As an example, here's the command you would run to ignore the =score= information : 

#+BEGIN_SRC sh
python3 redq.py "coronavirus" -abl "score" -f "xml"
#+END_SRC

Note that this is strictly equivalent to the following command :

#+BEGIN_SRC sh
python3 redq.py "coronavirus" -awl "content" "title" "comments" -f "xml"
#+END_SRC

And here's the result :

#+BEGIN_SRC xml
<xml>
    <post>
	<title>"Shoot them dead": Philippine President Rodrigo Duterte orders police and military to kill citizens who defy coronavirus lockdown</title>
	<comment>This guys really only has the one tool in his toolbox doesnt he</comment>
	<comment>This really is his catch-all solution isn't it??</comment>
	<comment>The crazy bastard's at it again.</comment>
    </post>
    <post>
	<title>The best thing to come from the coronavirus.... We adopted our foster pup!</title>
	<comment>11/10, would snug.</comment>
	<comment>Yay!!! Also his or her markings are so cool</comment>
	<comment>I love that this darling was adopted, but I feel I need to remind folks not to adopt (or especially buy pets) just because you're lonely.  I keep seeing a lot of posts from people showing off their newly acquired fur friend during this quarantine, and I can't help but wonder what will happen to them once life returns to normal.  Please remember that you've committed yourself to their survival.</comment>
    </post>
    <post>
	<title>(UK) Surgeon who was kicked out by his landlady over coronavirus infection fears sets up successful website matching vacant rooms with NHS workers in need</title>
	<comment>Users often report submissions from this site and ask us to ban it for sensationalized articles. At /r/worldnews, we oppose blanket banning any news source. Readers have a responsibility to be skeptical, check sources, and comment on any flaws.You can help improve this thread by linking to media that verifies or questions this article's claims. Your link could help readers better understand this issue. If you do find evidence that this article or its title are false or misleading, contact the moderators who will review itI am a bot, and this action was performed automatically. Please contact the moderators of this subreddit if you have any questions or concerns.</comment>
	<comment>...is that even legal?</comment>
	<comment>CovidBnB</comment>
    </post>
</xml>
#+END_SRC

* Python usage
** Basic usage
   redQ can also be used from python as a library. To perform a reddit search, you can use the /query_reddit/ function. It is defined like so :

 #+BEGIN_SRC python
 def query_reddit(
     query: str,
     user_agent: str,
     max_posts_nb: int,
     sort_type: str,
     attr_constructors: Optional[List[RedditPostAttrConstructor]] = None,
     **kwargs,
 ) -> List[RedditPost]:
     ...
 #+END_SRC

 Here is an example of a simple usage :

 #+BEGIN_SRC python
 from redq import query_reddit 
 from useragents import firefox_user_agent

 posts = query_reddit("my query", firefox_user_agent, 3, "sort")
 #+END_SRC

 This returns a list of =RedditPost=. A =RedditPost= is an object filled with informations about a reddit post. You can serialize a =RedditPost= using the convenience methods =to_xml()= and =to_json()=.

** Extension mechanisms
   redQ is an extensible program. When retrieving a =RedditPost= using =query_reddit=, it is constructed using a list of =RedditPostAttrConstructor=. Each =RedditPostAttrConstructor= is responsible for retrieving a specific information using the post web page (which is given as a =BeautifulSoup= object). It is a very simple interface, defined like so :

 #+BEGIN_SRC python
 class RedditPostAttrConstructor:
     def __init__(self, name: str, constructor: Callable[[BeautifulSoup, dict], Any]):
         self.name = name
         self.constructor = constructor

     def construct(self, page: BeautifulSoup, **kwargs) -> Any:
         return self.constructor(page, **kwargs)
 #+END_SRC

 which means you can retrieve custom informations by creating your own =RedditPostAttrConstructor= ! By default, the current =RedditPostAttrConstructor= list used by =query_reddit= is =all_attr_constructors=, defined in =post.py=. Currently, =all_attr_constructors= contains the following constructors :

 #+BEGIN_SRC python
 >>> from post import all_attr_constructors
 >>> [e.name for e in all_attr_constructors]
 ['title', 'content', 'score', 'comments']
 #+END_SRC 
 
But you can call =query_reddit= with your own custom list of =RedditPostAttrConstructor=, and you can receive arguments in your custom attribute constructor function using the =kwargs= dictionary, which will be passed from the =query_reddit= function.

 #+BEGIN_SRC python
from redq import query_reddit
from useragents import firefox_user_agent
from post import all_attr_constructors

def custom_constructor_fn(page: BeautifulSoup, **kwargs) -> Any:
    my_custom_arg = kwargs.get("my_custom_arg", None)
    #your attribute retrieval code here
my_attr_constructor = RedditPostAttrConstructor("custom", custom_constructor_fn)
custom_attr_constructors = all_attr_constructors + [my_attr_constructor]

posts = query_reddit("some query", firefox_user_agent, 3, "sort", custom_attr_constructors, my_custom_arg="foo")
 #+END_SRC 

As a bonus, the serialization functions =to_xml()= and =to_json()= of =RedditPost= should be modular enough to allow serializing most informations automatically.
