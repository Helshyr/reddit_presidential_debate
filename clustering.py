"""
MASSE Océane & DAUDRÉ--TREUIL Prunelle
Ce fichier contient tout le nécéssaire à la création des clusters 
dans la cadre de l'analyse de sujet des commentaires.
"""

################## IMPORTS

from tf_idf_clean import *

from sklearn.decomposition import PCA
from sklearn.cluster import KMeans

import matplotlib.pyplot as plt
import numpy as np
import heapq

import json

############################################ SCRIPT
#________ Création des clusters

"""
Pour réaliser mes clusters j'ai besoin de trouver le nombre de clusters à construire.
Pour trouver ce n optimal je vais devoir réaliser une PCA pour réduire la dimension de la matrice TF-IDF
afin de rendre visuelleement exploitable le graphique des scores pour chaque cluster.
"""

# La PCA, Analyse en composantes principales (Principal component analysis), est une transformation
# Nous allons ici baisser la dimension de notre matrice clairsemée X du fichier tf idf
# Il s'agit concrètement de baisser l'ensemble du vecteur vocabulaire en conservant son sens
# Il passe donc d'une taille "taile du vocs" à n_components
# Ainsi les distances entre les vecteurs sont conservées 
# X_pca aura pour dim (nbr commentaires, n_components)
# J'utilise .todense() pour passer de la forme clairsemée à la representation standard

pca = PCA(n_components=10)
X_pca = pca.fit_transform(X.todense())
#plt.scatter(X[:, 0], X_pca[:, 1], c=kmeans.labels_.astype(np.float))
#plt.show()

# Je vais mainteant fixer un nombre de clusters max arbitraires
# et réaliser le score k_means pour chaque nombre de cluster

max_clusters_nb = 20
scores = []
for k in range(1, max_clusters_nb):
    kmeans = KMeans(n_clusters=k, random_state=0)
    kmeans.fit(X_pca)
    scores.append(-kmeans.score(X_pca))

# Affichons le graphique à exploiter
plt.plot(scores)
plt.xlabel('Number of Clusters')
plt.ylabel('Score')
plt.title('Elbow Curve')
#plt.show()

"""
Nous déterminons graphiquement un n d'environ 10 ( asymptote de la courbe)
À présent, nous pouvons réaliser notre clustering, il s'agit de regrouper les
commentaires par similarités.

Le k_means clustering est un algorithme partionnement. Il s'agit d'un nuage de points
dans lequel on peu remarquer des similarités ou éloignements.
Cela consiste en initialiser des centroides ( ici de manière aléatoire par init = random)
Par calculs de barycentres, déplacement des centroîdes, re calculs des distances en boucle, 
les n clusters seront formés. 
n_init signifie que k_means sera initié 10 fois avec des centroides aléatoires.
"""

# On obtient bien la matrice de l'appartenance de chaque commentaires à un cluster
n = 10
kmeans = KMeans(init= 'random', n_clusters=n, n_init=10, random_state=0) 
kmeans.fit(X_pca)
com_clusters_matrix = kmeans.labels_

# Il faut maintenant regrouper les commentaires selon leurs clusters
clusters = []
for _ in range(n): # n_clusters
    clusters.append([])

for comment, label in zip(clean_csv_file["comments"], com_clusters_matrix):
    clusters[label - 1].append(comment)

# J'ordonne par ordre décroissant des tailles de clusters
decreasing_n_com_in_cluster = sorted(clusters, key=lambda x: len(x), reverse=True)

# À afficher et sauvegarder dans un fichier json pour plus de lisibilié au besoin

#print(json.dumps(decreasing_n_com_in_cluster, indent=4))



#________ Analyse des clusters



# Reprenons notre matrice clairsemée TF IDF de base

# Nous allons regrouper les commentaires sous forme de vecteurs selon leurs clusters
clusters_vectors = []

for _ in range(n): #n cluster
    clusters_vectors.append([])

for vector_comment, label in zip(X, com_clusters_matrix): 
    clusters_vectors[label - 1].append(vector_comment.todense())

# Maintenant nous allons calculer la moyenne de chaque cluster
# Ainsi nous aurons un vecteur représentant la moyenne du vocabulaire (cluster_mean)
# Nous en prendrons les n valeurs maximales (n_max) pour en faire une liste (idx_list)
# Puis nous faisons le lien entre l'indice de chaque max et le mot auquel il correspond
# On obtient bien les listes de sujet les plus courants pour chaque clusters

n_max = 15

for cluster in clusters_vectors:
    cluster_mean = np.array(cluster).mean(axis=0) # moy de chaque mot du cluster
    idx_list = heapq.nlargest(n_max , range(len(cluster_mean[0])), cluster_mean.take)
    list_vocab = []
    for i in idx_list:
        list_vocab.append(X_vocab[i])
#    print("cluster de card:", len(cluster))
#    print("sa list de voc:", list_vocab)

cluster_tri = []
for cluster in clusters_vectors:
    taille = len(cluster)
    cluster_mean = np.array(cluster).mean(axis=0) # moy de chaque mot du cluster
    moy_voc  = zip(cluster_mean[0], X_vocab)
    s = sorted(moy_voc, reverse=True)[:10]
    cluster_tri.append((taille, s))
    print("cluster", taille)
    print(s)
    print("\n")

cluster_tri = sorted(cluster_tri, reverse=True)

print(cluster_tri)



